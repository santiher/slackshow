# Slackshow

This is a few hours project to make a year summary/prank video simulating how
we use slack with our friends. Don't expect beautiful code!

## Live example

[Demo](https://santiher.gitlab.io/slackshow/)

## Custom emojis

In order to incorporate custom emojis just add the `emoji.png` file with the
emoji name in `emojis`. Note that emojis containing underscores can cause
markdown problems with italics.

## Profile pictures

Add jpg profile pictures in `profile_pics/username.jpg`.

## Script

You can write your script in `js/script.js`.

### Play script

The function to play your script is called at the bottom of `js/script.js`.

It receives three arguments: `play_script(elements, timed, show_is_typing)`

* elements: a list of script elements.
* timed: if set to true, the elements will be displayed as if it were a real
         conversation, otherwise it will add the chat messages one after the
         other right away.
* show_is_typing: if set to true, the *is typing* message will be shown for each
                  chat message

### Script elements

You can take a look at `js/script.js` and it should be pretty intuitive.

You can add a `delay` to elements, this is the amount of seconds it will wait
before processing the element.

#### Channel set up

Example:
```
{
    "active_channel": 'bots',
    "channels": ["general", "random", "bots"],
    "pinned_items": 2,
    "connected_members": ["slackbot", "cookie_monster"],
    "disconnected_members": ["lunch_train"],
    "delay": 1.0
}
```

#### Chat message

If a `delay` is not specified, the delay will be set automatically based on
the message's length. Examples:

```
{
    "user": "slackbot",
    "time": "12:50",
    "text": "Hello!",
    "reactions": [{"name": "joy", "n": 4}],
    "delay": 2.5
}
```

```
{
    "user": "slackbot",
    "time": "12:52",
    "text": `Welcome to my chat!`,
    "reactions": [],
}
```

#### Ghost is typing

You can add that someone is typing without sending a message, example:

```
{
    "user": "slackbot",
    "is_typing": true,
    "delay": 1.0,
    "duration": 17.0
}
```

## Disclaimers

* Markdown isn't perfect. Nested stuff, like bold inside code snippets or
  emojis containing underscores might not work properly.
* Line breaks: the best way is to add line breaks with a `<br>`.
* Images: you can add images using html (`<img src='images/your_image.jpg'>`).

## Resources

* Font icons: [icomoon.io](https://icomoon.io/)
* Emojis: [WebpageFx icons](https://github.com/WebpageFX/emoji-cheat-sheet.com)

## Colaborators

* [jpampliega](https://gitlab.com/jpampliega)
* [ldipasquale](https://gitlab.com/users/ldipasquale)
