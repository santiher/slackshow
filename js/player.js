var is_typing = []


function set_pinned_items(pinned_items) {
    pinned_spans =document.getElementsByClassName('pinned_items')
    for (var i = 0; i < pinned_spans.length; i++) {
        pinned_spans[i].innerHTML = pinned_items
    }
}


function fill_channels(channels, active_channel) {
    channel_spans =document.getElementsByClassName('channel_name')
    for (var i = 0; i < channel_spans.length; i++) {
        channel_spans[i].innerHTML = '#' + active_channel
    }
    var div = document.getElementById('channels_list')
    html = '<ul>'
    for (var i = 0 ; i < channels.length; i++) {
        if (channels[i] == active_channel) {
            html += '<li class="active_channel"># ' + channels[i] + '</li>'
        }
        else {
            html += '<li># ' + channels[i] + '</li>'
        }
    }
    html += '</ul>'
    div.innerHTML = html
}


function member_to_div(name, user_status, img_filter) {
    return '<div class="member"><img src="profile_pics/' + name +
           '.jpg" class="' + img_filter + '">' + name  +
           '<span class="'+ user_status + '">•</span></div>'
}


function fill_members(connected_members, disconnected_members) {
    var div = document.getElementById('channel_members')
    div.innerHTML = ''
    for (var i = 0 ; i < connected_members.length; i++) {
        div.innerHTML += member_to_div(connected_members[i], 'connected', '')
    }
    for (var i = 0 ; i < disconnected_members.length; i++) {
        div.innerHTML += member_to_div(disconnected_members[i], 'disconnected', 
                                       'disconnected_img')
    }
    var spans = document.getElementsByClassName('channel_members')
    for (var i = 0 ; i < spans.length; i++) {
        spans[i].innerHTML = (connected_members.length +
                              disconnected_members.length)
    }
    document.getElementsByClassName('online_channel_members')[0].innerHTML = connected_members.length
}


function change_channel(active_channel, channels, pinned_items,
                        connected_members, disconnected_members) {
    document.getElementById('chat_window').innerHTML = ''
    is_typing = []
    draw_is_typing()
    set_pinned_items(pinned_items)
    fill_channels(channels, active_channel)
    fill_members(connected_members, disconnected_members)
}


function revert_markdown(text) {
    new_line = /<br>/gm
    mention = /<b>(@.*?\s)<\/b>/g
    emoji = /<img.*? src="emojis\/(.*?).png">/gm
    bold = /<b>(.*?)<\/b>/g
    italic = /<em>(.*?)<\/em>/g
    strike = /`<strike>(.*?)<\/strike>`/gm
    text = text.replace(new_line, '\n')
    text = text.replace(mention, function(str, group) { return '<span class="link">' + group + '</span>' })
    text = text.replace(emoji, function(str, group) { return ':' + group + ':' })
    text = text.replace(bold, function(str, group) { return '*' + group + '*' })
    text = text.replace(italic, function(str, group) { return '_' + group + '_' })
    text = text.replace(strike, function(str, group) { return '~' + group + '~' })
    return text
}


function markdown(text) {
    mention = /(@.*?\s)/g
    emoji = /:(.*?):/g
    new_line = /\n/gm
    bold = /\*(.*?)\*/gm
    italic = /_(.*?)_/gm
    strike = /~(.*?)~/gm
    code = /```(.*?)```/gm
    variable = /`(.*?)`/gm
    text = text.replace(emoji, function(str, group) { return '<img width=18 height=18 style="position: relative; top: 3px" src="emojis/' + group + '.png">' })
    text = text.replace(new_line, '<br>')
    text = text.replace(mention, function(str, group) { return '<span class="link">' + group + '</span>' })
    text = text.replace(bold, function(str, group) { return '<b>' + group + '</b>' })
    text = text.replace(italic, function(str, group) { return '<em>' + group + '</em>' })
    text = text.replace(strike, function(str, group) { return '<strike>' + group + '</strike>' })
    text = text.replace(code, function(str, group) { return '<div class="snippet"><code>' + revert_markdown(group) + '</code></div>' })
    text = text.replace(variable, function(str, group) { return '<span class="variable">' + revert_markdown(group) + '</span>' })
    return text
}


function scroll_messages_down() {
    var div = document.getElementById('chat_window')
    div.scrollTop = div.scrollHeight;
}


function draw_is_typing() {
    div = document.getElementById('is_typing')
    var people = is_typing.length
    if (people == 0) {
        var text = ''
    }
    else {
        if (people == 1) {
            var p = is_typing[0]
            var j = ' is '
        }
        if (people > 1) {
            var p = (is_typing.slice(0, -1).join(', ') + ' and ' +
                     is_typing[people - 1])
            var j = ' are '
         }
        var text = '<em>' + p + j + 'typing </em>'
    }
    div.innerHTML = text
}


function remove_is_typing(user) {
    var index = is_typing.indexOf(user)
    if (index != -1) {
        is_typing.splice(index, 1);
        draw_is_typing()
    }
}


function add_is_typing(message) {
    var user = message['user']
    if (is_typing.indexOf(user) == -1) {
        is_typing.push(user)
        draw_is_typing()
        setTimeout(function(){ remove_is_typing(user) },
                   1000 *  message['duration']);
    }
}


function make_id() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0 ; i < 6 ; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
    return text;
}


function add_chat_message(message) {
    var reactions = []
    for (var i = 0 ; message['reactions'] && i < message['reactions'].length; i++) {
        var reaction = message['reactions'][i]
        var html = `
                <table class="reaction_emoji">
                  <tr>
                    <td>
                      <img src="emojis/` + reaction['name'] + `.png">
                    </td>
                    <td style="vertical-align: middle;">
                      <span>` + reaction['n'] + `</span>
                    </td>
                  </tr>
                </table>
                `
        reactions.push(html)
    }
    var random_id = "reaction_" + make_id();
    function show_reactions() {
        var reaction_element = document.getElementById(random_id)
        if (reaction_element) {
            reaction_element.style.visibility = 'visible'
        }
    }
    var template = `
              <div class="message">
                <table>
                  <tr>
                    <td class="chat_pic" rowspan="3">
                      <img src="profile_pics/` + message['user'] + `.jpg">
                    </td>
                    <td>
                      <span class="chat_user">` + message['user'] + `</span>
                      <span class="chat_time">` + message['time'] + `</span>
                    </td>
                  </tr>
                  <tr>
                    <td class="chat_text">` + markdown(message['text']) + `
                    </td>
                  </tr>
                  <tr>
                    <td class="chat_reactions" id="`+ random_id + `" style="visibility: hidden">` + reactions.join('') + `
                    </td>
                  </tr>
                </table>
              </div>
        `
    document.getElementById('chat_window').innerHTML += template
    setTimeout(show_reactions, 1000 * Math.random() + 1000);
}


function add_messages(messages) {
    document.getElementById('chat_window').innerHTML = ''
    for (var i = 0 ; i < messages.length; i++) {
        if (messages[i]['active_channel']) {
            change_channel(messages[i]['active_channel'],
                           messages[i]['channels'],
                           messages[i]['pinned_items'],
                           messages[i]['connected_members'],
                           messages[i]['disconnected_members'])
        }
        else if (messages[i]['is_typing']) {
            add_is_typing(messages[i])
        }
        else {
            add_chat_message(messages[i])
        }
    }
}


function add_messages_timed(messages, show_is_typing) {
    function calculate_delay(message) {
        if (message.hasOwnProperty('delay')) {
            return 1000 * message['delay']
        }
        words_per_minute = 180
        word_length = 5  // standardized number of chars in calculable word
        words = message['text'].length / word_length
        words_time = (words / words_per_minute) * 30 //60
        delay = 1.5  // seconds before people start reading
        bonus = 1    // extra time
        return 1000 * (delay + words_time + bonus)
    }
    function add_message_i(i) {
        if (messages[i - 1]['active_channel']) {
            change_channel(messages[i - 1]['active_channel'],
                           messages[i - 1]['channels'],
                           messages[i - 1]['pinned_items'],
                           messages[i - 1]['connected_members'],
                           messages[i - 1]['disconnected_members'])
        }
        else if (messages[i - 1]['is_typing']) {
            add_is_typing(messages[i - 1])
        }
        else {
            if (show_is_typing) {
                remove_is_typing(messages[i - 1]['user'])
            }
            add_chat_message(messages[i - 1])
            // Extra time to load the image(s)
            if (messages[i - 1]['text'].indexOf('<img') != -1) {
                setTimeout(scroll_messages_down, 0.1);
            }
            else {
                scroll_messages_down()
            }
        }
        if (i >= messages.length) {
            return
        }
        if (show_is_typing && messages[i]['text']) {
            user = messages[i]['user']
            if (is_typing.indexOf(user) == -1) {
                is_typing.push(user)
                draw_is_typing()
            }
        }
        delay = calculate_delay(messages[i - 1])
        setTimeout(function(){ add_message_i(i + 1) }, delay);
    }
    if (messages.length > 0) {
        add_message_i(1)
    }
}


function play_script(messages, timed, show_is_typing) {
    if (timed === false) {
        add_messages(messages)
    }
    else {
        add_messages_timed(messages, show_is_typing)
    }
}
